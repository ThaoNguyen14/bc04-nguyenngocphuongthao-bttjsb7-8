var numArr = [];
var plusNumArr = [];
var negativeNumArr = [];
var numArrmoi = [];
function themSo() {
  //   console.log("yes");
  //lấy số
  var number = document.getElementById("txt-number").value * 1;
  document.getElementById("txt-number").value = "";
  //luu vao mang
  numArr.push(number);

  if (number < 0) {
    negativeNumArr.push(number);
  } else {
    plusNumArr.push(number);
  }

  console.log({ numArr, plusNumArr, negativeNumArr });

  // 1.tong so duong
  var Tongsoduong = 0;
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    if (currentNum > 0) {
      Tongsoduong += currentNum;
    }
  }

  //2.dem so duong
  var Soluongsoduong = 0;
  var SoduongArr = numArr.filter(function (number) {
    return number > 0;
  });
  Soluongsoduong = SoduongArr.length;

  //3. tim so nho nhat
  minNum = numArr[0];
  for (var index = 0; index < numArr.length; index++) {
    var currentNum = numArr[index];
    var currentminNum = currentNum;
    if (minNum > currentminNum) {
      minNum = currentminNum;
    }
  }

  //4. tim so duong nho nhat

  minplusNum = plusNumArr[0];
  for (var index = 0; index < plusNumArr.length; index++) {
    var currentNum = plusNumArr[index];
    var currentminNum = currentNum;
    if (minplusNum > currentminNum) {
      minplusNum = currentminNum;
    }
  }
  //5. tim so chan cuoi cung
  Sochancuoicung = -1;
  socuoicung = numArr[numArr.length - 1];

  if (socuoicung % 2 == 0) {
    var Sochancuoicung = socuoicung;
  } else {
    for (var index = numArr.length - 1; index >= 0; index--) {
      var currentsocuoicung = numArr[index];
      if (currentsocuoicung % 2 == 0) {
        var Sochancuoicung = currentsocuoicung;
      }
    }
  }

  //7.sap xep theo thu tu tang dan
  numArr.sort(function (a, b) {
    return a - b;
  });

  //8. tim so nguyen to dau tien
  var songuyentodautien = numArr[0];
  for (var index = 0; index < numArr.length; index++) {
    var currentnumber = numArr[index];

    // so nguyen to: currentnumber>1 và không chia hết cho [2; Math.sqrt(currentnumber)]

    if (currentnumber > 1) {
      var b = Math.sqrt(currentnumber);
      for (var a = 2; a <= b; a++) {
        if (currentNum == 2) {
          songuyentodautien = currentnumber;
          break;
        }
        if (currentnumber % a != 0) {
          songuyentodautien = currentnumber;
          break;
        }
      }
    }
  }

  //10. so sanh so luong so am va so duong

  var result = "";

  if (negativeNumArr.length > plusNumArr.length) {
    result = "Số lượng số âm nhiều hơn số lượng số dương";
  }
  if (negativeNumArr.length < plusNumArr.length) {
    result = "Số lượng số âm ít hơn số lượng số dương";
  }
  if (negativeNumArr.length == plusNumArr.length) {
    result = "Số lượng số âm bằng số lượng số dương";
  }

  document.getElementById("result").innerHTML = ` Mảng : ${numArr}
  <br> Tổng số dương: ${Tongsoduong} <br>
  Số lượng số dương: ${Soluongsoduong} 
  <br> Số nhỏ nhất: ${minNum} 
  <br> Số dương nhỏ nhất: ${minplusNum} 
  <br> ${result} 
  <br> Số chẵn cuối cùng :${Sochancuoicung}
  <br> Số nguyên tố đầu tiên : ${songuyentodautien}
  `;
}

//9. thêm số mới và đếm số nguyên
function themSomoi() {
  //lấy số
  var numbermoi = document.getElementById("txt-number-moi").value * 1;
  document.getElementById("txt-number-moi").value = "";
  //luu vao mang
  numArrmoi.push(numbermoi);

  //và đếm số nguyên
  var Songuyen = 0;
  for (var index = 0; index < numArrmoi.length; index++) {
    var currentNum = numArrmoi[index];

    if (Number.isInteger(currentNum) == true) {
      Songuyen++;
    }
  }

  document.getElementById("resultmoi").innerHTML = ` Mảng : ${numArrmoi}
  <br> Số lượng số nguyên: ${Songuyen}`;
}

//6. đổi chỗ
function doicho() {
  let rs = "";
  var num1 = document.getElementById("num1").value * 1;
  var num2 = document.getElementById("num2").value * 1;
  var max = numArr.length;
  var temp;
  if (num1 > max || num2 > max) {
    rs = `Nhap so toi da la ${max}`;
  } else {
    temp = numArr[num1];
    numArr[num1] = numArr[num2];
    numArr[num2] = temp;
    rs = `Ket qua: ${numArr.toString()} <br> temp: ${temp}`;
  }
  console.log({ numArr, num1, num2 });
  document.getElementById("result-6").innerHTML = rs;
}
